package mkv.examples.ex2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, предназначенный для демонстрации метода incrementArrayElements
 * @author Малякин Кирилл. Гр. 15ОИТ20
 */
public class ArrayIncToTenPercentEx {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double[] exampleArr = {0.3,23,75,9.85,6.931};
        System.out.println("Array: " + Arrays.toString(exampleArr));
        System.out.print("Какой элемент нужно увеличить на 10 процентов (нумерация начинается с 0): ");
        int index = scanner.nextInt();
        incrementArrayElements(exampleArr,index,10);
        System.out.println("Modified array: " + Arrays.toString(exampleArr));
    }

    /**
     * Метод, увеличивающий каждый элемент массива на <code>incPercents</code>%
     * @param array Исходный массив
     * @param incPercents Количество процентов, на которое нужно увеличить массив.
     */
    private static void incrementArrayElements(double[] array,int index, double incPercents) {
        if (array.length >= index && index >= 0) {
            array[index] += array[index] / 100 * incPercents;
        }
    }
}
