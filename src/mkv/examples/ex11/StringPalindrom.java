package mkv.examples.ex11;

import java.util.Scanner;

/**
 * Класс демонстрирует работу метода isPalindrom, проверяющего, является ли введённое число палиндромом
 * @author Малякин К.В.
 */
public class StringPalindrom {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        String input;
        while (true) {
            System.out.print("Enter a word: ");
            input = scanner.next();
            System.out.println("\"" + input + "\" is " + (isPalindrom(input) ? "palindrom" : "not palindrom"));
        }
    }

    static boolean isPalindrom(String input) {
        return input.contentEquals(reverseString(input)); //Может и не слишком оптимизировано, но я не особо беспокоюсь.
    }

    static String reverseString(String input) {
        StringBuffer result = new StringBuffer();
        for (int i = input.length() - 1; i > -1; i--) {
            result.append(input.charAt(i));
        }
        return result.toString();
    }
}
