package mkv.examples.ex1;

import java.util.Scanner;

/**
 * Программа, которая считывает у пользователя строку, и читает её до точки, заодно выделяя количество пробелов.
 * @author Малякин Кирилл.  15ОИТ20.
 */
public class ReadStrToDotEx {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        String example = scanner.nextLine();
        int n = example.indexOf('.');
        String output = example;
        if (n > -1) {
            output = example.substring(0, n);
        }
        int j = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.charAt(i) == ' ') {
                j++;
            }
        }
        System.out.println("Input: " + example + "\nOutput: " + output + "\nCount of spaces in example: " + j);
    }

}
