package mkv.examples.ex9;

import java.util.Arrays;

/**
 * Класс, демонстрирующий работу метода eraseCol, который обнуляет определённый столбец прямоугольной матрицы.
 * @author Малякин Кирилл. Гр. 15ОИТ20
 */
public class MatrixColEraser {
    public static void main(String[] args) {
        int[][] matrix = {{1,2,3},
                          {4,5,6},
                          {7,8,9}};
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
        System.out.println("==============================");
        eraseCol(matrix,1);
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }

    }

    /**
     * Метод, обнуляющий определённый столбец матрицы <code>matrix</code>, номер которого передаётся в
     * <code>col</code>
     *
     * @param matrix Исходная матрица
     * @param col Номер целевого столбца
     */
    static void eraseCol(int[][] matrix, int col) {
        for (int i = 0; i < matrix.length; i++) {
            if (col > matrix[i].length) {
                //На всякий случай. Вдруг кто - то подсунет рваную матрицу.
                //Хотя в контексте данной задачи, никто такого делать не будет.
                throw new IndexOutOfBoundsException("Индекс столбца выходит за границы матрицы.");
            }
            matrix[i][col] = 0;
        }

    }
}
