package mkv.examples.ex6;

import java.util.Scanner;

/**
 * Программа, которая выводит таблицу умножения введенного пользователем числа с клавиатуры.
 *
 * <ol>Особенности:
 * <li>Eсли число меньше 10, то вычисляется вся таблица до 10, иначе вычисление идёт вплоть до введённого числа.</li>
 * <li>Число обязательно целое, иначе и быть не может.</li>
 * </ol>
 *
 * Естественно, защиты от дурака нет. Слишком уж это жирно для подобной программы, да ещё и в оригинальном задании
 * этого не было.. Так что...
 * @author Малякин К.В. гр. 15ОИТ20
 */

public class MultiplingTable {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        int number = scanner.nextInt();
        int maxLimit = ((number <= 10) ? 11 : number + 1); //Мэджик
        for (int i = 1; i < maxLimit; i++ ) {
            System.out.println(number + " x " + i + " = " + (number * i) + ";");
        }
    }
}
