package mkv.examples.ex15;

import java.util.Arrays;

/**
 * Медленная, не разу не оптимизированная реализация метода транспонирования дробной матрицы.
 * Почему? Причин много - и всё отговорки.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class MatrixTranspose {

    public static void main(String[] args) {
        int[][] matrix = new int[(int)randomRangeGen(2,5)][(int)randomRangeGen(2,5)];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = (int)randomRangeGen(-10,10);
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
        System.out.println("==============================");
        matrix = transposeMatrix(matrix); //Костыль. Адрес на матрицу почему - то не менялся
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }

    static int[][] transposeMatrix(int[][] matrix) {
        int[][] tempMatrix;
        //Проверим, не является ли матрица рваной.
        int h = matrix.length;
        int w = 0;
        if (h != 0) {
            w = matrix[0].length;
            for (int i = 1; i < matrix.length && w != 0; i++) {
                if (matrix[i].length != w) {
                    w = 0;
                }
            }
        }
        if (h == 0 | w == 0) {
            throw new IllegalArgumentException("Данный массив не является матрицей.");
        }
        tempMatrix = new int[w][h];
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < matrix.length; i++){
                tempMatrix[j][i] = matrix[i][j];
            }
        }
        return tempMatrix;
    }

    /**
     * Метод генерации псевдослучайного числа из заданного промежутка
     * @param min Нижняя граница для генератора
     * @param max Верхняя граница для генератора
     * @return Псевдослучайное число, от <code>min</code> до <code>max</code>
     */
    static double randomRangeGen(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }
}
