package mkv.examples.ex8;

import java.util.Scanner;

/**
 * Программа, определяющая, является введённое число целым, или же оно дробное.
 *
 * Никаких защит от дурака нет.
 * @author Малякин К.В. гр. 15ОИТ20
 */
public class IntegerNumber {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число, а мы проверим, является ли оно целым: ");
        double inputNumber = scanner.nextDouble();
        if (inputNumber != (int) inputNumber) {
            System.out.println("Это дробное число.");
        } else {
            System.out.println("Это целое число.");
        }
    }
}
