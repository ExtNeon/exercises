package mkv.examples.ex4;

/**
 * Created by Кирилл on 23.01.2017.
 * Расчет расстояния до места удара молнии. Звук в воздухе распространяется со скоростью приблизительно равной 1234,8
 * километров в час. Зная интервал времени между вспышкой молнии и звуком сопровождающим ее можно рассчитать расстояние.
 * Допустим интервал 6,8 секунды.
 */
public class ThunderDelay {
    final private static double delay = 6.8; //seconds
    final private static int speedOfSound = 331; //meters in second

    public static void main(String[] args) {
        System.out.print("Расстояние от места, куда ударила молния до места, где был услышан гром, если интервал между " +
                "вспышкой и звуком был равен " + delay + " секунл: " + formatPrefixToBigNumbers((int) delay * speedOfSound, "метров"));
    }

    private static String formatPrefixToBigNumbers(double number, String nameOfUnit) { //Как - то костыльно выглядит.
        String prefix = "";
        int multiplier = 1;
        if (number / 1000 >= 1) {
            prefix = "Кило";
            multiplier = 1000;
        }
        return (number / multiplier) + " " + prefix + nameOfUnit;
    }
}
