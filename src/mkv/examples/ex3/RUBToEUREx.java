package mkv.examples.ex3;

import java.util.Scanner;

/**
 * Created by Кирилл on 19.01.2017.
 */
public class RUBToEUREx {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double inputMoney;
        do {
            System.out.print("Введите количество рублей: ");
            inputMoney = scanner.nextDouble();
        } while (inputMoney <= 0);
        double convMlt;

        do {
            System.out.print("Сколько рублей стоит один евро: ");
            convMlt = scanner.nextDouble();
        } while (convMlt <= 0);
        System.out.println("Результат: " + valuteConversion(inputMoney,1/convMlt) + " евро.");
    }

    /**
     * Метод, переводящий одну валюту в другую.
     * @param inValute Исходная валюта
     * @param conversionMultipler Коэффициэнт преобразования.
     * @return Преобразованная валюта (<code>inValute * converionMultipler</code>)
     */
    private static double valuteConversion(double inValute, double conversionMultipler) {
        return inValute * conversionMultipler;
    }
}
