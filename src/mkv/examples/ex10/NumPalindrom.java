package mkv.examples.ex10;

/**
 * Created by Кирилл on 26.01.2017.
 * Напишите метод, который будет проверять является ли число палиндромом (одинаково читающееся в обоих направлениях).
 */
public class NumPalindrom {
    public static void main(String[] args) {
        System.out.println(isPalindrom(123321));
    }

    static boolean isPalindrom(int number) {
        return number == flipNumber(number);
    }

    static int flipNumber(int input){
        int i = 0;
        while (input >= 1) {
            i = i * 10 + input % 10;
            input /= 10;
        }
        return i;
    }

}
