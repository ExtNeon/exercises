package mkv.examples.ex13;

import java.util.Arrays;

/**
 * Из двумерного массива заполненного случайными числами перенесите построчно эти числа в одномерный массив.
 * Примечание: данный класс работает только с прямоугольными матрицами.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class TwoDimArrayToOneDim {

    public static void main(String[] args) {
        int[][] randomArray = new int[(int)randomRangeGen(2,10)][(int)randomRangeGen(2,10)];
        for (int i = 0; i < randomArray.length; i++) {
            for (int j = 0; j < randomArray[i].length; j++) {
                randomArray[i][j] = (int)randomRangeGen(2,10);
            }
        }
        for (int i = 0; i < randomArray.length; i++) {
            System.out.println(Arrays.toString(randomArray[i]));
        }
        System.out.println("\n");
        System.out.println(Arrays.toString(convertTo1D(randomArray)));
    }

    /**
     * Возвращает целочисленный массив, в который построчно перенесено содержимое двумерного целочисленного массива
     * @param input Входящий двумерный целочисленный массив
     * @return Целочисленный массив, в который построчно перенесено содержимое массива <code>input</code>
     */
    static int[] convertTo1D(int[][] input) {
        int [] targetArr = new int[input.length * input[0].length];
        int tempInt = 0;
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                targetArr[tempInt++] = input[i][j];
            }
        }
        return targetArr;
    }

    /**
     * Метод генерации псевдослучайного числа из заданного промежутка
     * @param min Нижняя граница для генератора
     * @param max Верхняя граница для генератора
     * @return Псевдослучайное число, от <code>min</code> до <code>max</code>
     */
    static double randomRangeGen(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }
}
