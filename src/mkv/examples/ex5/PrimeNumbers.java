package mkv.examples.ex5;

/**
 * Данный класс вычисляет простые числа от 2 до 100.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class PrimeNumbers {

    public static void main(String[] args) {
        for (int i = 2; i < 101; i++) {
            System.out.println(i + " is " +  (isPrime(i)?"prime":"not prime"));
        }
    }

    /**
     * Возвращает булево значение, равное истине, если число является простым, и ложь, если
     * @param number Исходное число
     * @return {@code true} если число простое, иначе {@code false}
     */
    private static boolean isPrime(int number) {
        boolean result = true;
        for (int i = 2; i < number && result; i++) {
            result = number % i != 0;
        }
        return result;
    }
}
