package mkv.examples.ex14;

import java.util.Scanner;

/**
 * Класс, демонстрирующий работу метода, определяющего, является ли введённый символ буквой, цифрой, или пунктуационным знаком.
 * @author Малякин Кирилл
 */
public class CheckSymbolType {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.print("Введите символ: ");
            System.out.println("Данный символ пренадлежит к группе \"" + getSymbolType(scanner.next().charAt(0)) + "\".");
        }
    }

    /**
     * Возвращает enum... Короче, определяет, чем является исходный символ: буквой, пунктуационным знаком, или же цифрой.
     * @param symbol Исходный символ
     * @return ...
     */
    static SymbolType getSymbolType(char symbol) {
        final String punctuationSymbols = ".,!?;\"#№$%^:&*()/+-=_`'\\<>{}[]|";
        final String digits = "0123456789";
        SymbolType result = SymbolType.LETTERS;
        for (int i = 0; i < punctuationSymbols.length() && result == SymbolType.LETTERS; i++) {
            if (punctuationSymbols.charAt(i) == symbol) {
                result = SymbolType.PUNCTUATION_SYMBOLS;
            }
        }
        for (int i = 0; i < digits.length() && result == SymbolType.LETTERS; i++) {
            if (digits.charAt(i) == symbol) {
                result = SymbolType.DIGITS;
            }
        }
        return result;
    }

    static enum SymbolType {
        DIGITS, LETTERS, PUNCTUATION_SYMBOLS
    }
}
