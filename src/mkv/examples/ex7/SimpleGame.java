package mkv.examples.ex7;

import java.util.Scanner;

/**
 * Этот класс представляет собой реализацию простейшей игры "Угадай число"
 *
 * Суть в том, что программа генерирует псевдослучайное число в диапазоне от 0 до 10, а пользователь должен это число
 * угадать, посредством ввода с клавиатуры. <br>
 * Наша версия игры дополнительно даёт подсказки пользователю, а именно: <ol>
 *     <li>Если введённое число больше случайно сгенерированного</li>
 *     <li>Если введённое число меньше случайно сгенерированного</li>
 * </ol>
 * Игра продолжается до тех пор, пока пользователь не угадает число.
 *
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class SimpleGame {
    static Scanner scanner = new Scanner(System.in);
    final static int minLimit = 0;
    final static int maxLimit = 10;


    public static void main(String[] args) {
        int number = (int) randomRangeGen(minLimit,maxLimit);
        int input;
        while (true) {
            System.out.print("Введите число, которое, по вашему мнению, загадала программа (от " + minLimit + " до " +
                    maxLimit + "): ");
            input = scanner.nextInt();
            if (input > number) {
                System.out.println("Увы, неверно. Введённое число больше загаданного.");
            } else if (input < number) {
                System.out.println("Увы, неверно. Введённое число меньше загаданного.");
            } else if (input == number) {
                System.out.println("Верно!");
                return;
            }
        }
    }

    /**
     * Метод генерации псевдослучайного числа из заданного промежутка
     * @param min Нижняя граница для генератора
     * @param max Верхняя граница для генератора
     * @return Псевдослучайное число, от <code>min</code> до <code>max</code>
     */
    static double randomRangeGen(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }
}
