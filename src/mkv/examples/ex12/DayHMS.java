package mkv.examples.ex12;

import java.util.Scanner;

/**
 * Created by Кирилл on 26.01.2017.
 * Напишите программу, которая будет считать количество часов, минут и секунд в n-ном количестве суток.
 */
public class DayHMS {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] string) {
        System.out.print("Введите количество суток: ");
        int daysCount = scanner.nextInt();
        int hours = 24 * daysCount;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println("In " + daysCount + " days: " + hours + " часов; " + minutes + " минут; " + seconds + " секунд.");

    }
}
